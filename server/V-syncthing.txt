===========================
Syncthing on Debian 9 Setup
===========================

Syncthing is an open, secure, and private file syncrhonization service that has
clients on many different platforms.

----------------
repository setup
----------------

Before installing, some system configuration is required.

Head to https://apt.syncthing.net/ to see the instructions on setting up the
syncthing repo on Debian.
	This is needed because Debian Stretch doesn't have a very up to date
	version of syncthing in the regular repository.

Run `curl -s https://syncthing.net/release-key.txt | sudo apt-key add -` to add
the pgp key for the repository.

Then, add `deb https://apt.syncthing.net/ syncthing stable` to
`/etc/apt/sources.list` 

Run, `sudo apt install apt-transport-https` to enable using https with apt.

Run `sudo apt update` to update your package lists.

----------------------
installation and setup
---------------------- 

Run `sudo apt install syncthing` to install syncthing.

By default, syncthing does not run as a daemon and must be started manually by
running `syncthing`. It is possible, however, to set up a systemd service for
syncthing.

Run `sudo systemctl enable syncthing@$USER.service` to enable it as a system
service.
	If a user service is desired, instead run `systemctl --user enable
	syncthing@$USER.service`

Then run `sudo systemctl start syncthing@$USER.service` to start it for the
first time without needing a restart.

=============
configuration
=============

-----------------
accessing web GUI
-----------------

Syncthing should now be working, and you should see the default `Sync`
directory that it creates. In order to test that everything is working and to
do some initial configuration, the web GUI is needed.
	Technically everything can be configured using the config file which is
	located at `~/.config/syncthing/config.xml` but it's way easier to just
	do it in the GUI. More reading about the config file can be found at
	`https://docs.syncthing.net/users/config.html`.

To access the web GUI from a different computer (if your server is currently
headless and without a display server), ssh tunneling can be used.

On another computer (with a monitor), run `ssh -L 9090:localhost:8384
ipofserver` (replace ipofserver with the ip of the computer running syncthing)
to establish a tunnel from your server with the port that the GUI listens on to
localhost with port 9090.

Once a tunnel has been established, open a web browser and go to
`localhost:9090`, the web GUI should be there.

--------------------
web GUI introduction
--------------------

On the web GUI, there are three main panels, folders, remote devices, and
statistics.

	Folder panel: This panel shows all the directories on your computer
	that are using syncthing, clicking on a folder will show some
	statistics for that directory and allows for configuration of the
	directory.

	Remote devices: This panel allows you to see all the other devices
	which are synced with this one, from this panel new devices can be
	added and settings for other devices can be changed.

	Statistics: This panel shows basic statistics about the current device
	that syncthing is running on.

Further reading about the web GUI can be found on the syncthing website:
`docs.syncthing.net/intro/gui.html`

It is a good idea to take some time to explore all the menus and settings
avaliable in the web GUI in order to familiarize yourself with the program.

------------------------
opening/forwarding ports
------------------------

To let syncthing work through your firewall, simply run `sudo ufw allow
syncthing` to open up the necessary ports that syncthing uses.

Port 22000 must also be forwarded in your routers settings to allow syncthing
to work without needing to use a relay.
