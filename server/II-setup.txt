===================================
Debian 9 Server Guide Part 2: Setup
===================================
 
This is part 2 of the Debian 9 server guide

In this part, I will be going over what initial steps you should take when
first setting up your server.

It will cover the tweaks that I use and some basic useful software to have.

Now a word of slight caution, I personally use vim as my text editor, if you
don't know what that is then instead of using 'vi' or 'vim' to edit text files,
you should probably use 'nano' instead.
	Nano is a pretty good text editor, so it should be easy enough to figure
	out even for new users.

Another important note, the manual is your friend. 'man' + a command will open
a manual page for that command. Man pages are agreat resource to learn about a
program or command in your system. Do not be afraid to read the manual for a
program that you don't know about.

=============
initial setup
=============

When you first boot your computer up you should be greeted with a login prompt.
From this prompt, first login as the 'root' user with the password you set
during the installation.

--------
updating
--------

Once you're logged in, run 'apt update' and then 'apt upgrade' to update the
system, there probably won't be too much here but it's good to do anyway.

---------------
git & etckeeper
---------------

Etckeeper is a really nice tool that will track every single change made in
/etc/ using git (it is possible to use other services to track changes but git
is default), which makes it very easy to view and undo any changes made to the
system's configuration files.

It is a good idea to install and configure git a little bit before installing
etckeeper.

Run 'apt install git' to install git.

Once installed, in order to set git up to work, you must give it a name and
email to use.
	Run 'git config --global user.email "you@example.com" and 'git config
	--global user.name "Your Name"' to set these up.
		Replace the "you@example.com" and "Your Name" with your desired email
		and name, respectively.

Run 'apt install etckeeper' to install etckeeper.
	If you set everything up correctly there should now be a directory called
	.git in /etc/

=======================
quality-of-life changes
=======================

These settings are completely optional but I'd highly reccomend them.

-----------
GRUB screen
-----------

Disable the GRUB boot selection screen that shows up for 5 seconds when your
computer starts up.

Run 'vi /etc/default/grub' and change the 'GRUB_TIMEOUT=' line to 0.

Run 'update-grub' to make the changes actually work.

------------------
message of the day
------------------

Clear out the default motd that is displayed when you log in to your computer.

Run 'echo "" > /etc/motd' to replace the file with nothing.
	It would probably be safer to just open this file with a text editor but as
	long as you type this command correctly it'll work. You can also open this
	file up in a text editor and make it something else if you'd like, but now
	there's a clean slate to start at.

=============================
non-free packages (sorry rms)
=============================

To enable the non-free repositories, edit your sources.list file found in
/etc/apt/

Run 'vi /etc/apt/sources.list' to open the file.

The first thing to do once inside this file is delete the CD repositories.
	Delete any lines starting with 'deb cdrom' to delete these repos.

Next, enable both the 'contrib' and 'non-free' repositories.
	For more info on what packages are in these repos, see
	https://wiki.debian.org/SourcesList#Component

	To add these repos, add 'contrib' and 'non-free' to the end of every line
	in the file.

Run 'apt update' after the changes have been made to update your package lists.

=================
sudo installation
=================

We need to install and setup 'sudo', a program that let's regular users run
commands as root (or technically any other user, but the most common usage is
as root).

Run 'apt install sudo' to install the package.

Run 'adduser name sudo' once it is installed finished.
	Replace 'name' with the username created when installing Debian.

Run 'logout' to log out from the root account and then log in as your regular
user.

===================
installing software
===================

Once logged in as a normal user with sudo privledges, it is time to install
more software.

Here is a list of all the packages that I would reccomend installing right now:

	firmware-linux: meta-package with lots of firmware for various devices that isn't in the kernel already

	neovim: A fork of the vim text editor that has some built-in plugins and is
	just really good.

	htop: Really great system/process monitor.
	
	build-essential: Tools to build programs from source (gcc, make, etc...).
	
	neofetch: System information tool that looks really nice and can get a lot
	of information depending on how you configure it.
	
	curl: Tool to transfer data from a website.
	
	wget: Another tool to transfer data from a website.
	
	needrestart: A program to check if a process needs to be restarted after
	you upgrade your system.
	
	tmux: An awesome terminal multiplexer, basically a wm for your terminal.
	
	ranger: Ncurses file manager.
	
	net-tools: Tools to control the network subsystem of the Linux kernel.
	
	python-pip: The python package manager, also includes python.
	
	python3-pip: The python 3 package manager, also includes python 3.

Run 'sudo apt install neovim htop build-essential neofetch curl wget
needrestart tmux ranger net-tools python-pip python3-pip' to install these
packages.

============
SSH security
============

Because this computer is going to be open to the internet, it it probably a
good idea to set up some security precautions first.

----------------------
disable root SSH login
----------------------

Start by configuring your SSH server to not allow anyone to login as root over SSH.

Run 'sudo vim /etc/ssh/sshd_config' to edit the SSH configuration file
	Find the line that has "PermitRootLogin", uncomment it, and change it to 'no'

Now run 'sudo systemctl restart sshd' to restart the server and apply your changes.

--------------
Fail2Ban setup
--------------

Fail2Ban is a very nice program that will ban anyone who tries to log in to
your server too many times.

To install it, run 'sudo apt install fail2ban'

Once it is installed, configuring the jail is necessary.
Run 'sudo vim /etc/fail2ban/jail.local' to create the configuration file
	To set the ban time, add '[DEFAULT]' to the first line and on the next
	line, add 'bantime = 31557600'. 'bantime' is measured in seconds, so this
	will set the time to 1 year. To ignore certain IP addresses, add 'ignoreip
	= 127.0.0.1/8' followed by any IP's you want to ignore under the
	'[DEFAULT]' section, use a space to separate each entry. To actually enable
	the jail, add '[sshd]' to a new line, followed by 'enabled = true' on the
	following line

Notice that there is a jail.conf in that same directory, this file contains the
defaults for fail2ban and should not be changed. Any changes to settings can
simply be done in the .local file by overriding the desired settings.
	If desired, more settings can be found by reading the man page for
	jail.conf and by looking at the jail.conf file (don't change it though) to
	find settings and default values.

Run 'sudo fail2ban-client reload' once it has been fully configured to apply
the changes.

=========
UFW setup
=========

'ufw', or the Uncomplicated FireWall, is a really simple and easy firewall that
can be used to block certain connections to the comptuer.

Run 'sudo apt install ufw' to install the package.

The following commands will enable/disable IP addresses and ports from
connecting:
	
	Run 'sudo ufw default allow outgoing' to allow outgoing connections from
	the server.
	
	Run 'sudo ufw default deny incoming' to disable all incoming connections to
	the server.
	
	Run 'sudo ufw allow 22' to enable port 22 (SSH) on incoming connections.
	
	Run 'sudo ufw allow from addr' and replace addr with any IP address you
	want to allow incoming connections on any port from said IP.
		Probably a good idea to allow your IP address so you don't screw
		yourself.

Run 'sudo ufw enable' once you've allowed/denied all the ports and IP addresses
you want.

Run 'sudo ufw logging on' to turn on logging for blocked packets.

============
finishing up
============

Run 'sudo reboot' to reboot your computer and make sure everything is working
after you've edited and installed lots of new softare.

After you've rebooted successfully, that's it! You can now continue on to the
next part.

===============================
Possible Missing Firmware Error
===============================

If there is ever a message like "possible missing firmware for XXXX" when booting your computer up or when installing packages, you may have a device whose firmware is not in the firmware-linux package or in the kernel itself

For example, if it mentions the device being a realtek device, try running 'sudo apt install firmware-realtek' and reboot to see if it fixes the issue
