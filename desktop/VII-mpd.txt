=====================
mpd and ncmpcpp setup
=====================

`mpd` or music player daemon, is a client/server solution for playing music on
a Linux system. There are a multitude of both terminal and GUI clients to
choose from when using `mpd`.

Run `sudo apt install mpd mpc` to install `mpd` and `mpc`, a powerful command
line `mpd` client.

On Debian mpd runs as a system daemon with systemd, this is most likely
undesirable unless you are trying to use mpd on a server, so some minor
configuration is required. The system service has a odd configuration (located
in `/etc/mpd.conf`) which seems unfriendly for a user. Running it as a user
service will load configuration files from `~/.config/mpd/` by default.

Run `sudo systemctl stop mpd.service mpd.socket` to stop the system service.

Then run `sudo systemctl disable mpd.service mpd.socket` to disable the system
service for mpd and it's socket and stop it from starting upon a reboot.

Then run `systemctl --user enable mpd.service mpd.socket` to enable the user
service.

Now that the service is set up correctly, a configuration file must be created
or else mpd will use the one in `/etc/mpd.conf`. This file must contain a few
lines for mpd to work correctly.

```
# Required lines
db_file "~/.config/mpd/database"
log_file "~/.config/mpd/log"

# Optional (but recommended) lines
pid_file "~/.config/mpd/pid"
state_file "~/.config/mpd/state"
sticker_file "~/.config/mpd/sticker.sql"

# Audio output, see `man mpd.conf` for more information
# pulseaudio example
audio_output {
	type "pulse"
	name "My pulseaudio output"
}
```

Run `systemctl --user start mpd.service` to start the service once the
configuration file has been created.

--------------
ncmpcpp client
--------------
