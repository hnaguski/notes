==========================
Neovim configuration guide
==========================

if not installed already, run `sudo apt install neovim` to install neovim

======
Vundle
======

Vundle is a plugin manager for vim which is quite nice.

Run `git clone https://github.com/VundleVim/Vundle.vim.git
~/.config/nvim/bundle/Vundle.vim` to clone the vundle repository into the nvim
config directory.

Then add the following lines to `~/.config/nvim/init.vim`:


```
" required for vundle (not sure why)
set nocompatible
filetype off

set rtp+=~/.config/nvim/bundle/Vundle.vim
call vundle#begin('~/.config/nvim/bundle')

" let vundle do vundle stuff (required)
Plugin 'VundleVim/Vundle.vim'

" plugins must be added before the next lines, required by vundle
call vundle#end()
filetype plugin indent on
```

Now save the file, and then start `vim` back up and run `:PluginInstall` to
initialize vundle.
